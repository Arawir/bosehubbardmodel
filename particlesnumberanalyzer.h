#ifndef PARTICLESNUMBERANALYZER
#define PARTICLESNUMBERANALYZER

#include "qbox/qbox.h"
#include "qbox/parameter.h"

struct ParticlesInNode
{
    ParticlesInNode(int position, double pn, double spn) :
        Position{position}
      , ParticlesNumber{pn}
      , SquareParticlenumber{spn}
      , Variation{0}
    {
        Variation = sqrt( SquareParticlenumber - ParticlesNumber*ParticlesNumber );
    }

    int Position;
    double ParticlesNumber;
    double SquareParticlenumber;
    double Variation;
};


void preapreParticlesNumberOperators(QBox& system, int numberOfNodes, IncreasingMode mode)
{
    if(mode==IncreasingMode::RL){
        for(int i=0; i<numberOfNodes/2; i++){
            system.addObservable( new Ni{i, Block::A, "na"+std::to_string(i), &system} );
            system.addObservable( new Ni{-i, Block::B, "nb-"+std::to_string(i), &system} );
        }

        for(int i=0; i<numberOfNodes/2; i++){
            system.addObservable( new N2i{i, Block::A, "n2a"+std::to_string(i), &system} );
            system.addObservable( new N2i{-i, Block::B, "n2b-"+std::to_string(i), &system} );
        }
    } else if(mode==IncreasingMode::RR){
        for(int i=0; i<numberOfNodes/2; i++){
            system.addObservable( new Ni{i, Block::A, "na"+std::to_string(i), &system} );
            system.addObservable( new Ni{i, Block::B, "nb"+std::to_string(i), &system} );
        }

        for(int i=0; i<numberOfNodes/2; i++){
            system.addObservable( new N2i{i, Block::A, "n2a"+std::to_string(i), &system} );
            system.addObservable( new N2i{i, Block::B, "n2b"+std::to_string(i), &system} );
        }
    }
}


std::vector<ParticlesInNode> getParticlesNumber(QBox& system, int numberOfNodes, IncreasingMode mode)
{
    std::vector<ParticlesInNode> OutputData;


    if(mode == IncreasingMode::RL){
        int position = 0;
        for(int i=0; i<numberOfNodes/2; i++){
            double ni = system.expectedValue("na"+std::to_string(i), 0);
            double n2i = system.expectedValue("n2a"+std::to_string(i), 0);
            OutputData.push_back( {position, ni, n2i} );
            position++;
        }

        for(int i=numberOfNodes/2-1; i>=0; i--){
            double ni = system.expectedValue("nb-"+std::to_string(i), 0);
            double n2i = system.expectedValue("n2b-"+std::to_string(i), 0);
            OutputData.push_back( {position, ni, n2i} );
            position++;
        }
    }

    if(mode == IncreasingMode::RR){
        int position = 0;
        for(int i=0; i<numberOfNodes/2; i++){
            double ni = system.expectedValue("na"+std::to_string(i), 0);
            double n2i = system.expectedValue("n2a"+std::to_string(i), 0);
            OutputData.push_back( {position, ni, n2i} );
            position++;
        }

        for(int i=0; i< numberOfNodes/2; i++){
            double ni = system.expectedValue("nb"+std::to_string(i), 0);
            double n2i = system.expectedValue("n2b"+std::to_string(i), 0);
            OutputData.push_back( {position, ni, n2i} );
            position++;
        }
    }

    return OutputData;
}

void showParticlesNumberDistribution(std::vector<ParticlesInNode>& nodesData)
{
    for(auto& node : nodesData){
        output //<< node.Position << "   "
               << node.ParticlesNumber << "   ";
              // << node.SquareParticlenumber << "   "
              // << node.Variation << "   "
              // << std::endl;
    }
    output << std::endl;
}

#endif // PARTICLESNUMBERANALYZER

