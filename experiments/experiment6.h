#ifndef EXPERIMENT6
#define EXPERIMENT6

#include "../particlesnumberanalyzer.h"

namespace Experiment6
{
    struct ExperimentConfig
    {
        IncreasingMode Mode = IncreasingMode::RL;
        uint NumberOfNodes = 10;
        uint NumberOfSweeps = 4;
        uint NodeRank = 4;
        uint BlockRank = 4;
        std::string PathToFile = "../data/experiment6/Jdata.txt";
    } Config;

    void configure(QBox& system)
    {
        system.setIncreasingMode(Config.Mode);
        system.setNodeRank(Config.NodeRank);
        system.setBlockRank(Config.BlockRank);
        system.parameter("J")->value() = 0.6;
        system.parameter("U")->value() = 1.0;
        system.parameter("Mi")->value() = 1.2;
        system.parameter("PeriodicBounadies")->value() = 0.0;
    }

    void saveData(std::vector<ParticlesInNode> particlesDistribution, double na, double nb, double nab)
    {
        std::fstream File(Config.PathToFile.c_str(), std::ios::out);

        double ParticlesSum = 0.0;

        for(auto& node : particlesDistribution){
            File << std::fixed << std::setprecision(10)
                 << node.Position << "   "
                 << node.ParticlesNumber << "   "
                 << node.SquareParticlenumber << "   "
                 << node.Variation << "   "
                 << std::endl;
            ParticlesSum += node.ParticlesNumber;
        }

        File << std::endl
             << ParticlesSum << "   "
             << na << "   "
             << nb << "   "
             << nab << std::endl;

        File.close();
    }


    void experimentB(QBox& system)
    {
        std::fstream File(Config.PathToFile.c_str(), std::ios::out);
        configure( system );

        double Mi = 3.64;
        double J = 0.0;
        double MaxJ = 0.72;
        double MaxMi = 4.001;
        double DeltaJ = 0.007;
        double DeltaMi = 0.04;


        while(Mi<=MaxMi){
            J=0.0;
            while(J<=MaxJ){
                output << std::endl << "J: " << J << " Mi: " << Mi << std::endl;
                system.parameter("J")->value() = J;
                system.parameter("Mi")->value() = Mi;
                system.newCycle();
                system.addNodesWithTruncation( Config.NumberOfNodes-2);
                system.sweeps(Config.NumberOfSweeps);

                File << std::fixed << std::setprecision(10)
                     << Mi << " "
                     << J << " "
                     << system.expectedValue("Nab",0) / static_cast<double>(Config.NumberOfNodes) << " "
                     << std::endl;

                J+=DeltaJ;
            }
            File << std::endl;
            Mi+=DeltaMi;
        }

        File.close();
    }

    void experimentC(QBox& system)
    {
        std::fstream File(Config.PathToFile.c_str(), std::ios::out);
        configure( system );

        double Mi = 0.8;
        double J = 0.0;
        double MaxJ = 0.72;
        double DeltaJ = 0.007;

        system.parameter("Mi")->value() = Mi;

        while(J<=MaxJ){
            output << std::endl << "J: " << J << " Mi: " << Mi << std::endl;
            system.parameter("J")->value() = J;

            system.newCycle();
            system.addNodesWithTruncation( Config.NumberOfNodes-2);
            system.sweeps(Config.NumberOfSweeps);

            File << std::fixed << std::setprecision(10)
                 << Mi << " "
                 << J << " "
                 << system.expectedValue("Nab",0) / static_cast<double>(Config.NumberOfNodes) << " "
                 << std::endl;

            J+=DeltaJ;
        }

        File.close();
    }


    void experimentA(QBox& system)
    {
        configure( system );
        system.newCycle();
        system.addNodesWithTruncation( Config.NumberOfNodes-2);
        system.sweeps(Config.NumberOfSweeps);

        auto ParticlesDistribution = getParticlesNumber(system, Config.NumberOfNodes, Config.Mode);
        double Na = system.expectedValue("Na",0);
        double Nb = system.expectedValue("Nb",0);
        double Nab = system.expectedValue("Nab",0);
        saveData(ParticlesDistribution, Na, Nb, Nab);
    }
}


#endif // EXPERIMENT6

