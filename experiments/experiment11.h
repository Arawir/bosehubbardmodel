#ifndef EXPERIMENT11
#define EXPERIMENT11

#include "../qbox/qbox.h"
#include "../qbox/parameter.h"
#include "../operators/correlation.h"
#include "../operators/nipartial.h"
#include "../operators/aedge.h"
#include "../operators/n.h"
#include "../operators/ni.h"
#include "../operators/n2i.h"
#include "../operators/bhhamiltonian.h"
#include "../operators/superblockhamiltonian.h"
#include "../operators/energy.h"
#include "../operators/momentum.h"

namespace Experiment11
{

    void stabilizeNumberOfNodes(QBox& system)
    {
        system.parameter("S")->value() = 0.0;
        system.sweepsToDeltaE(30, 0.1);
        system.parameter("S")->value() = 0.01;
        system.sweepsToDeltaE(30, 0.1);

        while(system.parameter("S")->value().real() < 1000.0){
            system.parameter("S")->value() *= 1.8;
            output << "S=" << system.parameter("S")->value() << std::endl;
            output << "<N> = " << system.expectedValue("Nab",0) << std::endl;
            system.sweepsToDeltaE(30, 0.1);
        }
        system.parameter("S")->value() = 0.0;
    }

    void ex1()
    {
        QBox system;
        system.setNodeRank(3);
        system.setBlockRank(6);
        system.setIncreasingMode(IncreasingMode::RL);

        system.addParameter( new Parameter{"J", 1.0} );
        system.addParameter( new Parameter{"U", 1.0} );
        system.addParameter( new Parameter{"Mi", 2.0} );
        system.addParameter( new Parameter{"PBC", 1.0} );
        system.addParameter( new Parameter{"WantedN", 16} );
        system.addParameter( new Parameter{"S", 0.0} );

        system.addObservable( new AEdge{EdgeType::Left, Block::A, "ala", &system} );
        system.addObservable( new AEdge{EdgeType::Right, Block::A, "ara", &system} );
        system.addObservable( new AEdge{EdgeType::Left, Block::B, "alb", &system} );
        system.addObservable( new AEdge{EdgeType::Right, Block::B, "arb", &system} );

        system.addObservable( new N{Block::A, "Na", &system} );
        system.addObservable( new N{Block::B, "Nb", &system} );
        system.addObservable( new N{Block::AB, "Nab", &system} );

        system.setHamiltonianA( new BHHamiltonian{Block::A, "Ha", &system} );
        system.setHamiltonianB( new BHHamiltonian{Block::B, "Hb", &system} );

        system.addObservable( new Energy{Block::AB, "Energy", &system} );
        system.setSuperblockHamiltonian( new SuperblockHamiltonian{Block::AB, "HAB", &system} );

       // system.parameter("Mi")->value() = 0.0;

////        std::fstream file("data", std::ios::out);
////        for(double g=0.0; g<10.01; g+=1.0){
//            system.parameter("J")->value() = 1.0/256.0;
//            system.parameter("U")->value() = g/16.0;
//            system.parameter("Mi")->value() = 2.0/256.0;

            system.newCycle();
            system.addNodesWithTruncation(16-2);
            output << "<N> bez stab = " << system.expectedValue("Nab",0) << std::endl;
            system.sweepsToDeltaE(30, 0.1);
           // stabilizeNumberOfNodes(system);

            output << "<N> = " << system.expectedValue("Nab",0) << std::endl;
            output << "<E> = " << system.expectedValue("Energy",0) << std::endl;
//            file << g << " "
//                   << system.expectedValue("Nab",0) << " "
//                   << system.expectedValue("Energy",0) << std::endl;
       // }
       // file.close();

    }

}

#endif // EXPERIMENT11

