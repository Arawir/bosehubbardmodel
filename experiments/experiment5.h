#ifndef EXPERIMENT5
#define EXPERIMENT5

#include "../particlesnumberanalyzer.h"

namespace Experiment5
{
//    struct ExperimentConfig
//    {
//        IncreasingMode Mode = IncreasingMode::JJ;
//        uint NumberOfNodes = 42;
//        uint NumberOfSweeps =3;
//        uint NodeRank = 3;
//        uint BlockRank = 6;
//        std::string PathToFile = "../data/experiment5/particlesDistribution.txt";
//    } Config;

//    void configure(QBox& system)
//    {
//        system.setIncreasingMode(Config.Mode);
//        system.setNodeRank(Config.NodeRank);
//        system.setBlockRank(Config.BlockRank);
//        system.parameter("J")->value() = 1.1;
//        system.parameter("U")->value() = 1.0;
//        system.parameter("Mi")->value() = 1.9;
//        system.parameter("PeriodicBounadies")->value() = 6.0;
//    }

//    void saveData(std::vector<ParticlesInNode> particlesDistribution, double na, double nb, double nab)
//    {
//        std::fstream File(Config.PathToFile.c_str(), std::ios::out);

//        double ParticlesSum = 0.0;

//        for(auto& node : particlesDistribution){
//            File << node.Position << "   "
//                 << node.ParticlesNumber << "   "
//                 << node.SquareParticlenumber << "   "
//                 << node.Variation << "   "
//                 << std::endl;
//            ParticlesSum += node.ParticlesNumber;
//        }

////        File << std::endl
////             << ParticlesSum << "   "
////             << na << "   "
////             << nb << "   "
////             << nab << std::endl;

//        File.close();
//    }

//    void experiment(QBox& system)
//    {
//        configure( system );
//        system.newCycle();
//        system.addNodes( Config.NumberOfNodes-2);
//        system.sweeps(Config.NumberOfSweeps);

//        auto ParticlesDistribution = getParticlesNumber(system, Config.NumberOfNodes, Config.Mode);
//        double Na = system.expectedValue("Na",0);
//        double Nb = system.expectedValue("Nb",0);
//        double Nab = system.expectedValue("Nab",0);
//        saveData(ParticlesDistribution, Na, Nb, Nab);
//    }
}

#endif // EXPERIMENT5

