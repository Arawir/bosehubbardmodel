#ifndef EXPERIMENT3
#define EXPERIMENT3

#include "particlesnumberanalyzer.h"

namespace Experiment3
{
    struct ExperimentConfig
    {
        IncreasingMode Mode = IncreasingMode::RL;
        uint NumberOfNodes = 100; //problem - to samo w mainie musi byc...
        uint NumberOfSweeps = 10;
        uint NodeRank = 4;
        uint BlockRank = 8;
    } Config;

    void configure(QBox& system)
    {
        system.setIncreasingMode(Config.Mode);
        system.setNodeRank(Config.NodeRank);
        system.setBlockRank(Config.BlockRank);
        system.parameter("PeriodicBounadies")->value() = 0.0;
    }

    std::vector<double> getEnergyLevels(QBox& system)
    {
        std::vector<double> EnergyLevels;
        for(uint i=0; i<system.currentBlockRank(); i++){
            EnergyLevels.push_back( system.expectedValue("Energy", i) );
        }
        return EnergyLevels;
    }

    void saveData(std::vector<double> energyLevels, std::vector<ParticlesInNode> particlesDistribution)
    {
        std::fstream FileE("../data/experiment3/energyLevels.txt", std::ios::out);
        std::fstream FileP("../data/experiment3/particlesDistribution.txt", std::ios::out);

        for(auto& energy : energyLevels){
            FileE << energy << std::endl;
        }

        for(auto& node : particlesDistribution){
            FileP << node.Position << "   "
                  << node.ParticlesNumber << "   "
                  << node.SquareParticlenumber << "   "
                  << node.Variation << "   "
                  << std::endl;
        }

        FileE.close();
        FileP.close();
    }

    void experiment(QBox& system)
    {
        configure(system);
        system.newCycle();
        system.addNodesWithTruncation(Config.NumberOfNodes);
        system.sweeps(Config.NumberOfSweeps);
        auto EnergyLevels = getEnergyLevels(system);
        auto ParticlesDistribution = getParticlesNumber(system, Config.NumberOfNodes, Config.Mode);
        saveData(EnergyLevels, ParticlesDistribution);
    }


}

#endif // EXPERIMENT3

