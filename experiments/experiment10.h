#ifndef EXPERIMENT10
#define EXPERIMENT10

#include <sys/stat.h>
#include "../qbox/qbox.h"
#include "../qbox/parameter.h"
#include "../operators/correlation.h"
#include "../operators/nipartial.h"

int exist(const char *name)
{
  struct stat   buffer;
  return (stat (name, &buffer) == 0);
}


namespace Experiment10
{
    struct ExperimentConfig
    {
        IncreasingMode Mode = IncreasingMode::RL;
        uint NumberOfNodes = 40;
        double WantedN = 40.0;
        uint NodeRank = 4;
        uint BlockRank = 4;
        std::string PathToFile;

        uint NodeCorrIPosition = 10;
        Block NodeCorrBlock = Block::A;
    } Config;

    void addCorrelationOperators(QBox& system)
    {
        for(int i=0; i<Config.NumberOfNodes/2; i++){
            system.addObservable( new Correlation_ij(Config.NodeCorrIPosition, i,
                                                     Config.NodeCorrBlock,
                                                     "Corr"+std::to_string(i),
                                                     &system) );

            system.addObservable( new Ni{i, Block::A, "na"+std::to_string(i), &system} );
            for(int j=0; j<Config.NodeRank; j++){
                system.addObservable( new NiPartial{j, i, Block::A, "p"+std::to_string(j)+"na"+std::to_string(i), &system} );
            }

            system.addObservable( new Ni{-i, Block::B, "nb"+std::to_string(-i), &system} );
            for(int j=0; j<Config.NodeRank; j++){
                system.addObservable( new NiPartial{j, -i, Block::B, "p"+std::to_string(j)+"na"+std::to_string(-i), &system} );
            }
        }

        for(int i=Config.NumberOfNodes/2; i<Config.NumberOfNodes; i++){
            system.addObservable( new Ni{i-static_cast<int>(Config.NumberOfNodes)+1, Block::B, "nb"+std::to_string(i-static_cast<int>(Config.NumberOfNodes)+1), &system} );
        }
    }

    void configure(QBox& system)
    {
        system.addParameter( new Parameter{"S", 0.0} );
        system.addParameter( new Parameter{"SMomentum", 0.0} );
        system.addParameter( new Parameter{"WantedN", Config.WantedN} );

        system.setIncreasingMode(Config.Mode);
        system.setNodeRank(Config.NodeRank);
        system.setBlockRank(Config.BlockRank);

        system.parameter("PeriodicBounadies")->value() = 6.0;
    }

    void saveData1(QBox &system)
    {
        std::fstream file(Config.PathToFile, std::ios::out);

        for(int i=0; i<Config.NumberOfNodes/2; i++){
            double corr = system.expectedValue("Corr"+std::to_string(i),0);
            double ni = system.expectedValue("na"+std::to_string(i),0);
            double nCorr = system.expectedValue("na"+std::to_string(Config.NodeCorrIPosition),0);

            file << i << " "
                 << ni << " ";

            for(int j=0; j<Config.NodeRank; j++){
                file << system.expectedValue("p"+std::to_string(j)+"na"+std::to_string(i),0) << " ";
            }

            file << corr << " "
                 << corr/ni/nCorr << " "
                 << std::endl;
        }

        for(int i=Config.NumberOfNodes/2; i<Config.NumberOfNodes; i++){
            file << i << " "
                 << system.expectedValue("nb"+std::to_string(i-static_cast<int>(Config.NumberOfNodes)+1),0) << " ";

            for(int j=0; j<Config.NodeRank; j++){
                file << system.expectedValue("p"+std::to_string(j)+"na"+std::to_string(i-static_cast<int>(Config.NumberOfNodes)+1),0) << " ";
            }


            file << std::endl;
        }

        file.close();

    }

    void stabilizeNumberOfNodesInternSweepActions(QBox& system)
    {
        system.parameter("S")->value() = 0.0;
        system.sweepsToDeltaE(30, 0.1);
        system.parameter("S")->value() = 0.01;

        system.addInternsweepAction(
                    [](iQBox *system) {
                        complex S = system->pII("S");
                        S *= 1.01;
                        system->parameter("S")->value() = S;
                        //complex P = system->parameter("PeriodicBounadies")->value();
                        //system->parameter("PeriodicBounadies")->value() = P*1.0016;
                    }
        );

        double particlesNumber = system.expectedValue("Nab",0);
        while( abs(particlesNumber-system.pII("WantedN") )>0.01){
            system.sweep();
            particlesNumber = system.expectedValue("Nab",0);
            double S = abs(system.pII("S"));
            output << "   " << "S=" << S << " N=" << particlesNumber << std::endl;
        }

        system.deleteInternSweepActions();
        system.sweepsToDeltaE(100, 0.1);
        system.parameter("S")->value() = 0.0;
    }

    void experimentA(QBox& system)
    {
        addCorrelationOperators(system);
        configure( system );

        double hBar = 1.0;
        double m = 1.0;
        double g = 8.0;
        double M = static_cast<double>(Config.NumberOfNodes);
        double delta = 1.0/M;

        double j = hBar*hBar/2.0/m/delta/delta;
        double u = 0.5*g*sqrt(32.0*j*hBar*hBar*m+m*m*g*g)/(4.0*hBar*hBar);

        system.parameter("Mi")->value() = 0.0;
        system.parameter("S")->value() = 0.0;
        system.parameter("J")->value() = j;
        system.parameter("SMomentum")->value() = 1.0;
        system.parameter("U")->value() = u;


        system.newCycle();
        system.addNodesWithTruncation( Config.NumberOfNodes-2);
        stabilizeNumberOfNodesInternSweepActions(system);
        //system.sweepsToDeltaE(60, 0.1);

        output << "N = " << system.expectedValue("Nab",0) << std::endl;

        Config.PathToFile = "../data/experiment10/S2g=" + std::to_string(g)
                           + "_N=" + std::to_string( system.expectedValue("Nab",0) )
                           + "_L=" + std::to_string( 1.0 )
                           + "_M=" + std::to_string( system.expectedValue("Mab",0) )
                           + "_E=" + std::to_string( system.expectedValue("Energy",0) );
        saveData1(system);

    }
}

#endif // EXPERIMENT10

