#ifndef DIAGRAM1
#define DIAGRAM1

#include "particlesnumberanalyzer.h"

void setParameters(QBox& system, double u, double j, double mi)
{
    output << "J: " << j << " Mi: " << mi << std::endl;
    system.parameter("U")->value() = u;
    system.parameter("J")->value() = j;
    system.parameter("Mi")->value() = mi;
}

void addNodes(QBox& system, int numberOfNodes)
{
    system.newCycle();
    for(int i=2; i<numberOfNodes; i+=2){
        system.addNodeWithTruncation();
    }
}

void saveDataToFile(std::fstream& file, std::vector<ParticlesInNode>& data, double j, double mi, int numberOfNodes)
{
    double AverrageParticlesNumber = 0.0;
    double AverrageSquareParticlesNumber = 0.0;
    double AverrageStandardDeviation = 0.0;

    double CalculatedNodes = 0.0;

    for(int i=numberOfNodes/8; i<numberOfNodes*3/8; i++){
        AverrageParticlesNumber+=data[i].ParticlesNumber;
        AverrageSquareParticlesNumber+=data[i].SquareParticlenumber;
        AverrageStandardDeviation+=data[i].Variation;
        CalculatedNodes+=1.0;
    }

    for(int i=numberOfNodes*5/8; i<numberOfNodes*7/8; i++){
        AverrageParticlesNumber+=data[i].ParticlesNumber;
        AverrageSquareParticlesNumber+=data[i].SquareParticlenumber;
        AverrageStandardDeviation+=data[i].Variation;
        CalculatedNodes+=1.0;
    }

    AverrageParticlesNumber /= CalculatedNodes;
    AverrageSquareParticlesNumber /= CalculatedNodes;
    AverrageStandardDeviation /= CalculatedNodes;

//    for(auto& node : data){
//        AverrageParticlesNumber+=node.ParticlesNumber;
//        AverrageSquareParticlesNumber+=node.SquareParticlenumber;
//        AverrageStandardDeviation+=node.Variation;
//    }

//    AverrageParticlesNumber /= data.size();
//    AverrageSquareParticlesNumber /= data.size();
//    AverrageStandardDeviation /= data.size();


    file << std::fixed << std::setprecision(10)
         << j << "   "
         << mi << "   "
         << AverrageParticlesNumber << "   "
         << AverrageSquareParticlesNumber << "   "
         << AverrageStandardDeviation << "   "
         << std::endl;
}

void sweepSystemMultipleTimes(QBox& system, int numberOfSweeps)
{
    for(int i=0; i<numberOfSweeps; i++){
        output << "  Sweep: " << i << std::endl;
        system.sweep();
    }
}


void prepareDiagram1(QBox& system, int numberOfNodes, IncreasingMode mode)
{
    //to set by user
    double J_min = 0.6, J_max = 0.8, J_delta = 0.02;
    double Mi_min = 0.0, Mi_max = 4.0, Mi_delta = 0.1;
    int NumberOfSweeps = 0;

    //do not touch
    std::fstream File("../data/diagram1D", std::ios::out);
    double J = J_min, Mi = Mi_min, U = 1.0;

    while(J<=J_max){
        while(Mi<=Mi_max){
            setParameters(system, U, J, Mi);
            addNodes(system, numberOfNodes);
            sweepSystemMultipleTimes(system, NumberOfSweeps);
            std::vector<ParticlesInNode> Data = getParticlesNumber(system, numberOfNodes, mode);
            saveDataToFile(File, Data, J, Mi, numberOfNodes);

            Mi+=Mi_delta;
        }
        File << std::endl;
        Mi = Mi_min;
        J+=J_delta;
    }

    File.close();
}

void prepareDiagram1Line(QBox& system, int numberOfNodes, IncreasingMode mode)
{
    //to set by user
    double J_min = 0.0, J_max = 0.8, J_delta = 0.01;
    double Mi = 4.0;
    int NumberOfSweeps = 20;

    //do not touch
    std::fstream File("../data/diagram1L_40", std::ios::out);
    double J = J_min, U = 1.0;

    while(J<=J_max*1.01){
        setParameters(system, U, J, Mi);
        addNodes(system, numberOfNodes);
        sweepSystemMultipleTimes(system, NumberOfSweeps);
        std::vector<ParticlesInNode> Data = getParticlesNumber(system, numberOfNodes, mode);
        saveDataToFile(File, Data, J, Mi, numberOfNodes);
        J+=J_delta;
    }

    File.close();
}

#endif // DIAGRAM1

