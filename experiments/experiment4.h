#ifndef EXPERIMENT4
#define EXPERIMENT4

#include "particlesnumberanalyzer.h"

namespace Experiment4
{
    struct ExperimentConfig
    {
        IncreasingMode Mode = IncreasingMode::RL;
        uint NumberOfNodes = 10; //problem - to samo w mainie musi byc...
        uint NumberOfSweeps = 3;
        uint NodeRank = 4;
        uint NodesToAddAtStart = 0;
        uint BlockRankAtStart = 4;
        uint BlockRankNormal = 4;
        std::string PathToFile = "../data/experiment4/particlesDistribution.txt";
    } Config;

    void configure(QBox& system)
    {
        system.setIncreasingMode(Config.Mode);
        system.setNodeRank(Config.NodeRank);
        system.setBlockRank(Config.BlockRankAtStart);
        system.parameter("PeriodicBounadies")->value() = 0.0;
    }

    void saveData(std::vector<ParticlesInNode> particlesDistribution)
    {
        std::fstream File(Config.PathToFile.c_str(), std::ios::out);

        for(auto& node : particlesDistribution){
            File << node.Position << "   "
                 << node.ParticlesNumber << "   "
                 << node.SquareParticlenumber << "   "
                 << node.Variation << "   "
                 << std::endl;
        }

        File.close();
    }

    void experiment(QBox& system)
    {
        configure( system );
        system.newCycle();
        system.addNodesWithTruncation( Config.NodesToAddAtStart);
        system.setBlockRank( Config.BlockRankNormal );
        system.addNodesWithTruncation( Config.NumberOfNodes-Config.NodesToAddAtStart-2);
        system.sweeps(Config.NumberOfSweeps);

        auto ParticlesDistribution = getParticlesNumber(system, Config.NumberOfNodes, Config.Mode);
        saveData(ParticlesDistribution);
    }
}

#endif // EXPERIMENT4

