#ifndef EXPERIMENT8
#define EXPERIMENT8

#include "../particlesnumberanalyzer.h"

namespace Experiment8
{
    struct ExperimentConfig
    {
        IncreasingMode Mode = IncreasingMode::RL;
        uint NumberOfNodes = 4;
        uint NumberOfSweeps = 4;
        uint NodeRank = 4;
        uint BlockRank = 4;
        std::string PathToFile = "../data/experiment8/momentum2.txt";
    } Config;

    void configure(QBox& system)
    {
        system.addParameter( new Parameter{"S", 0.0} );
        system.addParameter( new Parameter{"SMomentum", 0.0} );
        system.addParameter( new Parameter{"WantedN", 0.0} );

        system.setIncreasingMode(Config.Mode);
        system.setNodeRank(Config.NodeRank);
        system.setBlockRank(Config.BlockRank);

        system.parameter("PeriodicBounadies")->value() = 0.0;
        system.parameter("WantedN")->value() = 10.0;
        system.parameter("S")->value() = 0.0;
    }

    void saveDataB(double gamma, double e, double energy, double n, double rho)
    {
        std::fstream File(Config.PathToFile.c_str(), std::ios::app);

        File << gamma << "   "
             << e << "   "
             << energy << "   "
             << n << "   "
             << rho << "   "
             << std::endl;

        File.close();
    }

    void saveDataC(double gamma, double e, double energy, double n, double rho, double momentum)
    {
        std::fstream File(Config.PathToFile.c_str(), std::ios::app);

        File << gamma << " "
             << e << " "
             << energy << " "
             << n << " "
             << rho << " "
             << momentum << " "
             << std::endl;

        File.close();
    }

    void saveData2(double gamma, double energy, double n, double rho)
    {
        std::fstream File(Config.PathToFile.c_str(), std::ios::app);


        File << gamma << " "
             << energy/n/rho/rho << " "
             << energy << " "
             << n << " "
             << rho << " "
             << std::endl;

        File.close();
    }

    double calculateGamma(double U, double J, double M, double N)
    {
        double gamma = (U/2.0 * sqrt(1.0/J) * M) / N;
        return gamma;
    }

    double calculateRho(double j, double m, double n)
    {
        double rho = n/m/sqrt(j); //zmienić definicje
        return rho;
    }



    void stabilize(QBox& system)
    {
        double S = 0.0;
        double particlesNumber;
        system.parameter("S")->value() = 0.0;
        system.sweepsToDeltaE(10, 0.1);

        S = 1.0;
        particlesNumber = system.expectedValue("Nab",0);

        while(abs(particlesNumber-10.0)>0.01){
            system.parameter("S")->value() = S;
            system.sweepsToDeltaE(10, 0.1);
            particlesNumber = system.expectedValue("Nab",0);
            output << "   " << "S=" << S << " N=" << particlesNumber << std::endl;
            S *= 2.0;
        }
        system.parameter("S")->value() = 0.0;
    }

    void stabilizeMomentum(QBox& system)
    {
        double SMomentum = 0.0;
        double momentum;
        system.parameter("SMomentum")->value() = 0.0;
        system.sweepsToDeltaE(30, 0.1);

        SMomentum = 1.0;
        momentum = system.expectedValue("Mab",0);

        while(abs(momentum-6.0)>0.01){
            system.parameter("SMomentum")->value() = SMomentum;
            system.sweepsToDeltaE(30, 0.1);
            momentum = system.expectedValue("Mab",0);
            output << "   " << "SMomentum=" << SMomentum << " M=" << momentum << std::endl;
            SMomentum *= 2.0;
        }
        system.parameter("SMomentum")->value() = 0.0;
    }

    void experimentA(QBox& system)
    {
        configure( system );
        double j = 1.0;
        double mi = 2.0*j;
        system.parameter("J")->value() = j;
        system.parameter("Mi")->value() = mi;

        system.parameter("S")->value() = 0.0;

        double u = 0.1;
        double uMax = 1.0;
        double uDelta = 0.1;

        while(u<(uMax+uDelta/2.0)){
            output << "U=" << u << std::endl;
            system.parameter("U")->value() = u;
            system.newCycle();
            system.addNodesWithTruncation( Config.NumberOfNodes-2);
            stabilize(system);

            double energy = system.expectedValue("Energy",0);
            double n = system.expectedValue("Nab",0);
            double gamma = calculateGamma(u,j,Config.NumberOfNodes,n);
            double rho = calculateRho(j,Config.NumberOfNodes,n);

            saveData2(gamma, energy, n, rho);

            u+= uDelta;
        }
    }

    void experimentB(QBox& system)
    {
        configure( system );

        double hBar = 1.0;
        double m = 1.0;
        double delta = 0.025;
        double M = static_cast<double>(Config.NumberOfNodes);


        system.parameter("Mi")->value() = 0.0;
        system.parameter("S")->value() = 0.0;

        double g = 101.0;
        double gMax = 200.0;
        double gDelta = 1.0;

        while(g<(gMax+gDelta/2.0)){
            output << "g=" << g << std::endl;

            double j = hBar*hBar/2.0/m/delta/delta;
            double u = 0.5*g*sqrt(32.0*j*hBar*hBar*m+m*m*g*g)/(4.0*hBar*hBar);



            system.parameter("J")->value() = j;
            system.parameter("S")->value() = 0.0;

            system.parameter("U")->value() = u;
            system.newCycle();
            system.addNodesWithTruncation( Config.NumberOfNodes-2);
            //system.sweepsToDeltaE(10, 0.1);
            stabilize(system);

            double energy = system.expectedValue("Energy",0);
            double n = system.expectedValue("Nab",0);

            double rho = n/(M*delta);
            double gamma = g*m/(hBar*hBar*rho);
            double e = energy/(n*rho*rho);

            saveDataB(gamma, e, energy, n, rho);

            g+= gDelta;
        }
    }

    void experimentC(QBox& system)
    {
        configure( system );

        double hBar = 1.0;
        double m = 1.0;
        double delta = 0.025;
        double M = static_cast<double>(Config.NumberOfNodes);


        system.parameter("Mi")->value() = 0.0;
        system.parameter("S")->value() = 0.0;

        double g = 1.0;
        double gMax = 20.0;
        double gDelta = 1.0;

        while(g<(gMax+gDelta/2.0)){
            output << "g=" << g << std::endl;

            double j = hBar*hBar/2.0/m/delta/delta;
            double u = 0.5*g*sqrt(32.0*j*hBar*hBar*m+m*m*g*g)/(4.0*hBar*hBar);



            system.parameter("J")->value() = j;
            system.parameter("S")->value() = 0.0;

            system.parameter("U")->value() = u;
            system.newCycle();
            system.addNodesWithTruncation( Config.NumberOfNodes-2);
            //system.sweepsToDeltaE(10, 0.1);
            stabilize(system);

            double energy = system.expectedValue("Energy",0);
            double n = system.expectedValue("Nab",0);

            double rho = n/(M*delta);
            double gamma = g*m/(hBar*hBar*rho);
            double e = energy/(n*rho*rho);
            double momentum = system.expectedValue("Mab",0);

            saveDataC(gamma, e, energy, n, rho, momentum);

            g+= gDelta;
        }
    }


    void experimentD(QBox& system)
    {
        configure( system );

        double hBar = 1.0;
        double m = 1.0;
        double delta = 0.025;
        double M = static_cast<double>(Config.NumberOfNodes);


        system.parameter("Mi")->value() = 0.0;
        system.parameter("S")->value() = 0.0;
        system.parameter("SMomentum")->value() = 10.0;

        double g = 1000.0;
        double gMax = 1000.0;
        double gDelta = 1.0;

        while(g<(gMax+gDelta/2.0)){
            output << "g=" << g << std::endl;

            double j = hBar*hBar/2.0/m/delta/delta;
            double u = 0.5*g*sqrt(32.0*j*hBar*hBar*m+m*m*g*g)/(4.0*hBar*hBar);



            system.parameter("J")->value() = j;
            system.parameter("U")->value() = u;
            system.newCycle();
            system.addNodesWithTruncation( Config.NumberOfNodes-2);
            output << "momentum 0" << system.expectedValue("Mab",0) <<std::endl;
           // stabilizeMomentum(system);

            //system.sweepsToDeltaE(30, 0.1);

            for(int i=0; i<100; i++){
                system.sweep();
                output << "energy " << system.expectedValue("Energy",0) << "  "
                       << "BHH " << system.expectedValue("HAB",0) << "  "
                       << "momentum " << system.expectedValue("Mab",0) <<std::endl;
            }

            double energy = system.expectedValue("Energy",0);
            double n = system.expectedValue("Nab",0);

            double rho = n/(M*delta);
            double gamma = g*m/(hBar*hBar*rho);
            double e = energy/(n*rho*rho);
            double momentum = system.expectedValue("Mab",0);

            output << "momentum k" << system.expectedValue("Mab",0) <<std::endl;
            saveDataC(gamma, e, energy, n, rho, momentum);

            g+= gDelta;
        }
    }

}

#endif // EXPERIMENT8

