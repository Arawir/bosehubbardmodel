#ifndef DIAGRAM2
#define DIAGRAM2

#include "particlesnumberanalyzer.h"

void saveDataToFile(int numberOfNodes, std::vector< std::vector<ParticlesInNode> >& particlesInNodes )
{
    std::fstream File("../data/diagram2_4", std::ios::out);

    for(int i=0; i<numberOfNodes; i++){
        for(auto& data : particlesInNodes){
            File << data[i].ParticlesNumber << "   ";
        }
        File << std::endl;
    }

    File.close();
}

void prepareDiagram2(QBox& system, int numberOfNodes, IncreasingMode mode)
{
    std::vector< std::vector<ParticlesInNode> > ParticlesInNodes;

    system.setIncreasingMode(mode);
    system.newCycle();

    for(int i=2; i<numberOfNodes; i+=2){
        output << "NoN: " << i << std::endl;
        system.addNodeWithTruncation();
    }

    for(int i=0; i<201; i++){
        output << "SWEEP: " << i << std::endl;
        if(i%1 == 0){
            ParticlesInNodes.push_back( getParticlesNumber(system, numberOfNodes, mode) );
            saveDataToFile(numberOfNodes, ParticlesInNodes);
        }
        system.sweep();
    }
}

#endif // DIAGRAM2

