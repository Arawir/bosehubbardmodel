#ifndef EXPERIMENT7
#define EXPERIMENT7


#include "../particlesnumberanalyzer.h"

namespace Experiment7
{
    struct ExperimentConfig
    {
        IncreasingMode Mode = IncreasingMode::RL;
        uint NumberOfNodes = 40;
        uint NumberOfSweeps = 4;
        uint NodeRank = 4;
        uint BlockRank = 4;
        std::string PathToFile = "../data/experiment7/data.txt";
    } Config;

    void configure(QBox& system)
    {
        system.setIncreasingMode(Config.Mode);
        system.setNodeRank(Config.NodeRank);
        system.setBlockRank(Config.BlockRank);
        system.parameter("PeriodicBounadies")->value() = 1.0;
    }

    void saveData(double gamma, double e, double ewm, double n, double rho)
    {
        std::fstream File(Config.PathToFile.c_str(), std::ios::app);


        File << gamma << "   "
             << e << "   "
             << ewm << "   "
             << n << "   "
             << rho << "   "
             << e/n/rho/rho << " "
             << ewm/n/rho/rho << " "
             << std::endl;

        File.close();
    }

    void saveData2(double gamma, double energy, double n, double rho)
    {
        std::fstream File(Config.PathToFile.c_str(), std::ios::app);


        File << gamma << " "
             << energy/n/rho/rho << " "
             << energy << " "
             << n << " "
             << rho << " "
             << std::endl;

        File.close();
    }

    double calculateGamma(double U, double J, double M, double N)
    {
        double gamma = (U/2.0 * sqrt(1.0/J) * M) / N;
        return gamma;
    }

    double calculateE(double energy, double j, double m, double n)
    {
        double e = energy; //(energy*m*m)/(j*n*n*n);
        return e;
    }

    double calculateRho(double j, double m, double n)
    {
        double rho = n*n*j/m/m;
        return rho;
    }

    void experimentC(QBox& system)
    {
        configure( system );
        double j = 1.0;
        double mi = 2.0*j;
        system.parameter("J")->value() = j;
        system.parameter("Mi")->value() = mi;

        double u = 0.1;
        double uMax = 20.0;
        double uDelta = 0.1;

        while(u<(uMax+uDelta/2.0)){
            output << "U=" << u << std::endl;
            system.parameter("U")->value() = u;
            system.newCycle();
            system.addNodesWithTruncation( Config.NumberOfNodes-2);
            system.sweeps(Config.NumberOfSweeps);

            double energy = system.expectedValue("Energy",0);
            double n = system.expectedValue("Nab",0);
            double gamma = calculateGamma(u,j,Config.NumberOfNodes,n);
            double rho = calculateRho(j,Config.NumberOfNodes,n);

            saveData2(gamma, energy, n, rho);

            u+= uDelta;
        }
    }

}

#endif // EXPERIMENT7

