project(BoseHubbard_DMRG)
cmake_minimum_required(VERSION 2.8)

set(CMAKE_CXX_STANDARD 11)

set(SOURCES
    main.cpp)
set(HEADERS
    qbox/qbox.h
    qbox/basicphisics.h
    qbox/ioperator.h
    qbox/iparameter.h
    qbox/iqbox.h
    qbox/lanczos.h
    qbox/linearalgebra.h
    qbox/operator.h
    qbox/parameter.h
    qbox/algebraicobject.h
    qbox/operrep.h
    qbox/repository.h
    qbox/mathbox.h
    qbox/statebox.h
    qbox/numberbox.h

    operators/aedge.h
    operators/n.h
    operators/ni.h
    operators/n2i.h
    operators/bhhamiltonian.h
    operators/superblockhamiltonian.h
    operators/momentum.h
    operators/energy.h
    operators/correlation.h
    operators/nipartial.h

    basicmath.h
    particlesnumberanalyzer.h

    experiments/diagram1.h
    experiments/diagram2.h
    experiments/experiment3.h
    experiments/experiment4.h
    experiments/experiment5.h
    experiments/experiment6.h
    experiments/experiment7.h
    experiments/experiment8.h
    experiments/experiment9.h
    experiments/experiment10.h
    experiments/experiment11.h
)

#set(EXECUTABLE_OUTPUT_PATH "bin")

add_executable(${PROJECT_NAME} ${SOURCES} ${HEADERS})
target_include_directories(${PROJECT_NAME} PUBLIC include)

target_link_libraries(${PROJECT_NAME} ${GTEST_LIBRARIES} pthread armadillo lapack blas)
