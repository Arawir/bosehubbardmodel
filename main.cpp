#include <iostream>
#include <string>


#include "operators/aedge.h"
#include "operators/n.h"
#include "operators/ni.h"
#include "operators/n2i.h"
#include "operators/bhhamiltonian.h"
#include "operators/superblockhamiltonian.h"
#include "operators/energy.h"
#include "operators/momentum.h"

#include "particlesnumberanalyzer.h"


#include "experiments/experiment11.h"


void prepareParameters(QBox& system)
{
    system.setNodeRank(4);
    system.setBlockRank(4);
    system.addParameter( new Parameter{"J", 0.1} );
    system.addParameter( new Parameter{"U", 1.0} );
    system.addParameter( new Parameter{"Mi", 1.0} );
    system.addParameter( new Parameter{"m", 1.0} );
    system.addParameter( new Parameter{"PeriodicBounadies", 0.0} );
}

void prepareOperators(QBox& system)
{
    system.addObservable( new AEdge{EdgeType::Left, Block::A, "ala", &system} );
    system.addObservable( new AEdge{EdgeType::Right, Block::A, "ara", &system} );
    system.addObservable( new AEdge{EdgeType::Left, Block::B, "alb", &system} );
    system.addObservable( new AEdge{EdgeType::Right, Block::B, "arb", &system} );




    system.addObservable( new N{Block::A, "Na", &system} );
    system.addObservable( new N{Block::B, "Nb", &system} );
    system.addObservable( new N{Block::AB, "Nab", &system} );

    system.addObservable( new Momentum{Block::A, "Ma", &system} );
    system.addObservable( new Momentum{Block::B, "Mb", &system} );
    system.addObservable( new Momentum{Block::AB, "Mab", &system} );

    system.setHamiltonianA( new BHHamiltonian{Block::A, "Ha", &system} );
    system.setHamiltonianB( new BHHamiltonian{Block::B, "Hb", &system} );

    system.addObservable( new Energy{Block::AB, "Energy", &system} );
    system.setSuperblockHamiltonian( new SuperblockHamiltonian{Block::AB, "HAB", &system} );

    //preapreParticlesNumberOperators(system, numberOfNodes, mode);
}


//void lanczosTest1()
//{
//    arma::
//}


int main()
{
//    QBox System;

//    prepareParameters(System);
//    prepareOperators(System);

//    Experiment10::experimentA(System);

    Experiment11::ex1();

    return 0;
}

