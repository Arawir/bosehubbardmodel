#ifndef ENERGY
#define ENERGY

#include "../qbox/operator.h"

class Energy : public Operator
{
public:
    Energy(Block block, std::string name, iQBox *box) :
        Operator{block, name, box}
    {

    }

    void recalculate()
    {
        arma::cx_mat Ha = oII("Ha");
        arma::cx_mat Hb = oII("Hb");
        arma::cx_mat Ia = I(dimm(Ha));
        arma::cx_mat Ib = I(dimm(Hb));

        complex J = pII("J");
        arma::cx_mat ara = oII("ara");
        arma::cx_mat arb = oII("arb");
        arma::cx_mat alb = oII("alb");
        arma::cx_mat ala = oII("ala");

        complex P = pII("PBC");

        arma::cx_mat Hi = -J*( (ara.t()^alb) + (ara^alb.t()) )
                        + P*(-J)*( (ala.t()^arb) + (ala^arb.t()) );

        this->mat() = (Ha^Ib) + Hi + (Ia^Hb);
    }
};

#endif // ENERGY

