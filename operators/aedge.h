#ifndef AEDGE
#define AEDGE

#include "../qbox/operator.h"
#include "../basicmath.h"


enum class EdgeType
{
    Left, Right
};


class AEdge : public Operator
{
public :
    AEdge(EdgeType edge, Block block, std::string name, iQBox* box) :
        Operator{block, name, box}
      , Edge{edge}
    {

    }
    ~AEdge() = default;

    void increaseLeft() override
    {
        if(Edge==EdgeType::Left){
            arma::cx_mat an = a(Box->nodeRank());
            arma::cx_mat It = I(this->dim());
            this->mat() = an^It;
        } else {
            arma::cx_mat at = this->mat();
            arma::cx_mat In = I(Box->nodeRank());
            this->mat() = In^at;
        }
    }

    void increaseRight() override
    {
        if(Edge==EdgeType::Right){
            arma::cx_mat an = a(Box->nodeRank());
            arma::cx_mat It = I(this->dim());
            this->mat() = It^an;
        } else {
            arma::cx_mat at = this->mat();
            arma::cx_mat In = I(Box->nodeRank());
            this->mat() = at^In;
        }
    }

    void newCycle() override
    {
        this->mat() = a(Box->nodeRank());
        Operator::newCycle();
    }

private:
    EdgeType Edge;
};

#endif // AEDGE

