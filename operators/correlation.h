#ifndef CORRELATION
#define CORRELATION

#include "../qbox/operator.h"

class Correlation_ij : public Operator
{
public:
    Correlation_ij(int position_i, int position_j, Block block, std::string name, iQBox *box) :
        Operator{block, name, box}
      , PositionI{position_i}
      , PositionJ{position_j}
    {

    }

    ~Correlation_ij() = default;

    void increaseLeft()
    {
        arma::cx_mat m_n = n(Box->nodeRank());
        arma::cx_mat m_n2 = n2(Box->nodeRank());
        arma::cx_mat m_I = I(Box->nodeRank());


        if( (Box->currentlyAddedNodeNumber(this->block()) == PositionI) && (Box->currentlyAddedNodeNumber(this->block()) == PositionJ)){
            this->mat() = m_n2^(this->mat());
        } else if(Box->currentlyAddedNodeNumber(this->block()) == PositionI){
            this->mat() = m_n^(this->mat());
        } else if(Box->currentlyAddedNodeNumber(this->block()) == PositionJ){
            this->mat() = m_n^(this->mat());
        } else {
            this->mat() = m_I^(this->mat());
        }
    }

    void increaseRight()
    {
        arma::cx_mat m_n = n(Box->nodeRank());
        arma::cx_mat m_n2 = n2(Box->nodeRank());
        arma::cx_mat m_I = I(Box->nodeRank());

        if( (Box->currentlyAddedNodeNumber(this->block()) == PositionI) && (Box->currentlyAddedNodeNumber(this->block()) == PositionJ)){
            this->mat() = (this->mat())^m_n2;
        } else if(Box->currentlyAddedNodeNumber(this->block()) == PositionI){
            this->mat() = (this->mat())^m_n;
        } else if(Box->currentlyAddedNodeNumber(this->block()) == PositionJ){
            this->mat() = (this->mat())^m_n;
        } else {
            this->mat() = (this->mat())^m_I;
        }
    }

    void newCycle()
    {
        arma::cx_mat m_n = n(Box->nodeRank());
        arma::cx_mat m_n2 = n2(Box->nodeRank());
        arma::cx_mat m_I = I(Box->nodeRank());

        if( (Box->currentlyAddedNodeNumber(this->block()) == PositionI) && (Box->currentlyAddedNodeNumber(this->block()) == PositionJ)){
            this->mat() = m_n2;
        } else if(Box->currentlyAddedNodeNumber(this->block()) == PositionI){
            this->mat() = m_n;
        } else if(Box->currentlyAddedNodeNumber(this->block()) == PositionJ){
            this->mat() = m_n;
        } else {
            this->mat() = m_I;
        }

        Operator::newCycle();
    }

    int PositionI;
    int PositionJ;
};



#endif // CORRELATION

