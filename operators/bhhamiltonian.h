#ifndef BHHAMILTONIAN_H
#define BHHAMILTONIAN_H

#include "../qbox/operator.h"
#include "../basicmath.h"


class BHHamiltonian : public Operator
{
public:
    BHHamiltonian(Block block, std::string name, iQBox *box) :
        Operator{block, name, box}
    {

    }

    ~BHHamiltonian() = default;

    void increaseLeft()
    {
        arma::cx_mat t_H = this->mat();
        arma::cx_mat n_I = I(Box->nodeRank());
        arma::cx_mat t_I = I(this->dim());
        arma::cx_mat n_a = a(Box->nodeRank());
        arma::cx_mat n_Nn1 = nn1(Box->nodeRank());
        arma::cx_mat n_N = n(Box->nodeRank());

        complex J = pII("J");
        complex U = pII("U");
        complex Mi = pII("Mi");

        this->mat()  = n_I^t_H;
        this->mat() += U*(n_Nn1^t_I);
        this->mat() -= Mi*(n_N^t_I);

        if(this->block() == Block::A){
            arma::cx_mat ala = oI("ala");
            this->mat() -= J*( (n_a^trans(ala)) + (trans(n_a)^ala) );
        } else if(this->block() == Block::B){
            arma::cx_mat alb = oI("alb");
            this->mat() -= J*( (n_a^trans(alb)) + (trans(n_a)^alb) );
        }
    }

    void increaseRight()
    {
        arma::cx_mat t_H = this->mat();
        arma::cx_mat n_I = I(Box->nodeRank());
        arma::cx_mat t_I = I(this->dim());
        arma::cx_mat n_a = a(Box->nodeRank());
        arma::cx_mat n_Nn1 = nn1(Box->nodeRank());
        arma::cx_mat n_N = n(Box->nodeRank());

        complex J = pII("J");
        complex U = pII("U");
        complex Mi = pII("Mi");

        this->mat()  = t_H^n_I;
        this->mat() += U*(t_I^n_Nn1);
        this->mat() -= Mi*(t_I^n_N);

        if(this->block() == Block::A){
            arma::cx_mat ara = oI("ara");
            this->mat() -= J*( (trans(ara)^n_a) + (ara^trans(n_a)) );
        } else if(this->block() == Block::B){
            arma::cx_mat arb = oI("arb");
            this->mat() -= J*( (trans(arb)^n_a ) + (arb^trans(n_a)) );
        }
    }

    void newCycle()
    {
        arma::cx_mat Nn1 = nn1(Box->nodeRank());
        arma::cx_mat N = n(Box->nodeRank());
        complex U = pI("U");
        complex Mi = pI("Mi");

        this->mat() = U*Nn1 - Mi*N;

        Operator::newCycle(); //dodane
    }
};

#endif // BHHAMILTONIAN_H

