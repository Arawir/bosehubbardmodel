#ifndef MOMENTUM
#define MOMENTUM

#include "../qbox/operator.h"

class Momentum : public Operator
{
public:
    Momentum(Block block, std::string name, iQBox *box) :
        Operator{block, name, box}
    {

    }

    ~Momentum() = default;

    void increaseLeft()
    {
        if(this->block()!=Block::AB){
            complex J = pII("J");
            complex m = pII("m");
            complex P = sqrt(J*m/2.0)*im;

            arma::cx_mat M_t = this->mat();
            arma::cx_mat I_n = I(Box->nodeRank());
            arma::cx_mat a_n = a(Box->nodeRank());
            arma::cx_mat a_left;

            if(this->block() == Block::A){
                a_left = oI("ala");
            } else if(this->block() == Block::B){
                a_left = oI("alb");
            }

            this->mat() = (I_n^M_t) + P*(trans(a_n)^a_left) - P*(a_n^trans(a_left));
        }
    }

    void increaseRight()
    {
        if(this->block()!=Block::AB){
            complex J = pII("J");
            complex m = pII("m");
            complex P = sqrt(J*m/2.0)*im;

            arma::cx_mat M_t = this->mat();
            arma::cx_mat I_n = I(Box->nodeRank());
            arma::cx_mat a_n = a(Box->nodeRank());
            arma::cx_mat a_right;

            if(this->block() == Block::A){
                a_right = oI("ara");
            } else if(this->block() == Block::B){
                a_right = oI("arb");
            }

            this->mat() = (M_t^I_n) - P*(a_right^trans(a_n)) + P*(trans(a_right)^a_n);
        }
    }

    void recalculate()
    {
        if(this->block()== Block::AB){
            complex J = pII("J");
            complex m = pII("m");
            complex P = sqrt(J*m/2.0)*im;
            //std::cout << "P: " << P << std::endl;
            complex PeriodicBoundaries = pII("PBC");

            arma::cx_mat Ma = oII("Ma");
            arma::cx_mat Mb = oII("Mb");
            arma::cx_mat Ia = I(dimm(Ma));
            arma::cx_mat Ib = I(dimm(Mb));

            arma::cx_mat ara = oII("ara");
            arma::cx_mat arb = oII("arb");
            arma::cx_mat alb = oII("alb");
            arma::cx_mat ala = oII("ala");

            arma::cx_mat Mi = P*(trans(ara)^alb) - P*(ara^trans(alb))
                    + PeriodicBoundaries*( P*(trans(arb)^ala) - P*(arb^trans(ara)) );

            this->mat() = (Ma^Ib) + Mi + (Ia^Mb);

        }
    }

    void newCycle() override
    {
        this->mat() = Zero(Box->nodeRank());
    }
};

#endif // MOMENTUM

