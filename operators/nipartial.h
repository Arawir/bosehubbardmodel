#ifndef NIPARTIAL
#define NIPARTIAL

#include "../qbox/operator.h"
#include "../basicmath.h"

class NiPartial : public Operator
{
public:
    NiPartial(int numberOfParticles, int position, Block block, std::string name, iQBox *box) :
        Operator{block, name, box}
      , NumberOfParticles{numberOfParticles}
      , Position{position}
    {

    }

    ~NiPartial() = default;

    void increaseLeft()
    {
        arma::cx_mat m_n = nPartial(Box->nodeRank(),NumberOfParticles);
        arma::cx_mat m_I = I(Box->nodeRank());

        if(Box->currentlyAddedNodeNumber(this->block()) == Position){
            this->mat() = m_n^(this->mat());
        } else {
            this->mat() = m_I^(this->mat());
        }
    }

    void increaseRight()
    {
        arma::cx_mat m_n = nPartial(Box->nodeRank(),NumberOfParticles);
        arma::cx_mat m_I = I(Box->nodeRank());

        if(Box->currentlyAddedNodeNumber(this->block()) == Position){
            this->mat() = (this->mat())^m_n;
        } else {
            this->mat() = (this->mat())^m_I;
        }
    }

    void newCycle()
    {
        arma::cx_mat m_n = nPartial(Box->nodeRank(),NumberOfParticles);
        arma::cx_mat m_I = I(Box->nodeRank());

        if(Box->currentlyAddedNodeNumber(this->block()) == Position){
            this->mat() = m_n;
        } else {
            this->mat() = m_I;
        }
    }

public:
    int NumberOfParticles;
    int Position;
};

#endif // NIPARTIAL

