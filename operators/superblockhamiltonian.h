#ifndef SUPERBLOCKHAMILTONIAN
#define SUPERBLOCKHAMILTONIAN

#include "../qbox/operator.h"

class SuperblockHamiltonian : public Operator
{
public:
    SuperblockHamiltonian(Block block, std::string name, iQBox *box) :
        Operator{block, name, box}
    {

    }

    ~SuperblockHamiltonian() = default;

    void recalculate()
    {
        arma::cx_mat Ha = oII("Ha");
        arma::cx_mat Hb = oII("Hb");
        arma::cx_mat Ia = I(dimm(Ha));
        arma::cx_mat Ib = I(dimm(Hb));

        complex J = pII("J");
        arma::cx_mat ara = oII("ara");
        arma::cx_mat arb = oII("arb");
        arma::cx_mat alb = oII("alb");
        arma::cx_mat ala = oII("ala");
        complex P = pII("PBC");

        arma::cx_mat Hi = (-J)*( (ara^alb.t()) + (ara.t()^alb) )
                        + P*(-J)*( (ala^arb.t()) + (ala.t()^arb) );


        arma::cx_mat Nab = oII("Nab");
        //arma::cx_mat Mab = oII("Mab");
        arma::cx_mat Inab = I(dimm(Nab));
        complex S = pII("S");
//        complex SMomentum = pII("SMomentum");
        complex WantedN = pII("WantedN");
        arma::cx_mat Hs = S*(Nab-WantedN*Inab)*(Nab-WantedN*Inab);


        this->mat() = (Ha^Ib) + Hi + (Ia^Hb) /*-SMomentum*Mab*/ + Hs;
    }
};

#endif // SUPERBLOCKHAMILTONIAN

