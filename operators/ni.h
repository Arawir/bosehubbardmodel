#ifndef NI
#define NI

#include "../qbox/operator.h"
#include "../basicmath.h"

class Ni : public Operator
{
public:
    Ni(int position, Block block, std::string name, iQBox *box) :
        Operator{block, name, box}
      , Position{position}
      , CurrentNode{0}
    {

    }

    ~Ni() = default;

    void increaseLeft()
    {
        CurrentNodeSotrage.push_back(CurrentNode);
        CurrentNode--;

        arma::cx_mat t_Ni = this->mat();
        arma::cx_mat n_n = n(Box->nodeRank());
        arma::cx_mat n_I = I(Box->nodeRank());

        if(Position==CurrentNode){
            this->mat() = n_n^t_Ni;
        } else {
            this->mat() = n_I^t_Ni;
        }
    }

    void increaseRight()
    {
        CurrentNodeSotrage.push_back(CurrentNode);  
        CurrentNode++;
        arma::cx_mat t_Ni = this->mat();
        arma::cx_mat n_n = n(Box->nodeRank());
        arma::cx_mat n_I = I(Box->nodeRank());

        if(Position==CurrentNode){
            this->mat() = t_Ni^n_n;
        } else {
            this->mat() = t_Ni^n_I;
        }
    }

    void decrease() override
    {
        Operator::decrease();
        CurrentNode = CurrentNodeSotrage.back();
        CurrentNodeSotrage.pop_back();
    }

    void newCycle()
    {
        CurrentNode = 0;
        if(Position==CurrentNode){
            this->mat() = n(Box->nodeRank());
        } else {
            this->mat() = I(Box->nodeRank());
        }
        Operator::newCycle(); //dodane
    }
public:
    int Position;
    std::vector<int> CurrentNodeSotrage;
    int CurrentNode;
};

#endif // NI

