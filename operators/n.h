#ifndef N_
#define N_

#include "../qbox/operator.h"
#include "../basicmath.h"


class N : public Operator
{
public:
    N(Block block, std::string name, iQBox *box) :
        Operator{block, name, box}
    {

    }

    ~N() = default;

    void increaseLeft()
    {
        arma::cx_mat Nt = this->mat();
        arma::cx_mat nn = n(Box->nodeRank());
        arma::cx_mat It = I(this->dim());
        arma::cx_mat In = I(Box->nodeRank());
        this->mat() = (In^Nt) + (nn^It);
    }

    void increaseRight()
    {
        arma::cx_mat Nt = this->mat();
        arma::cx_mat nn = n(Box->nodeRank());
        arma::cx_mat It = I(this->dim());
        arma::cx_mat In = I(Box->nodeRank());
        this->mat() = (Nt^In) + (It^nn);
    }

    void recalculate()
    {
        if(block()==Block::AB){
            arma::cx_mat Na = oII("Na");
            arma::cx_mat Nb = oII("Nb");
            arma::cx_mat Ia = I(dimm(oII("Na")));
            arma::cx_mat Ib = I(dimm(oII("Nb")));
            this->mat() = (Na^Ib) + (Ia^Nb);
        }
    }

    void newCycle()
    {
        this->mat() = n(Box->nodeRank());
        Operator::newCycle();
    }
};

#endif // N_

