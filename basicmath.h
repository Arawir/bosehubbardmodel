#ifndef BASICMATH
#define BASICMATH

arma::cx_mat a(int rank)
{
    arma::cx_mat a(rank, rank, arma::fill::zeros);
    for(int i=0; i<rank-1; i++){
        a(i, i+1) = sqrt(i+1);
    }
    return a;
}

arma::cx_mat n(int rank)
{
    return a(rank).t()*a(rank);
}

arma::cx_mat nPartial(int rank, int numberOfParticles)
{
    arma::cx_mat Selector(rank, rank, arma::fill::zeros);
    Selector(numberOfParticles, numberOfParticles) = 1.0;
    return Selector; //rename
}

arma::cx_mat n2(int rank)
{
    return n(rank)*n(rank);
}

arma::cx_mat nn1(int rank)
{
    return n(rank)*(n(rank)-I(rank));
}

arma::cx_mat Zero(int rank)
{
    arma::cx_mat z(rank, rank, arma::fill::zeros);
    return z;
}

#endif // BASICMATH

